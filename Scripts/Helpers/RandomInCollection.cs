﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomInCollection {

    public static T getRandomInList<T>(IList<T> coll)
    {
        int index = (int)Random.Range(0f, coll.Count);
        return coll[index];
    }
	
}
