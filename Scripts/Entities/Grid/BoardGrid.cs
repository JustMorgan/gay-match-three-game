﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardGrid : MonoBehaviour {

    public Vector2 dim;
    public float squareSize;
    public Vector2 boardOffset;

	public Vector2 getScreenPos(Piece piece)
    {
        Vector2 retPos = new Vector2(squareSize * piece.boardPos.x + boardOffset.x, squareSize * piece.boardPos.y + boardOffset.y);

        return retPos;
    }

    public void populateBoard(List<Piece> pieces)
    {
        for(int x = 0; x < dim.x; x++)
        {
            for(int y = 0; y < dim.y; y++)
            {
                Piece piece = Instantiate<Piece>(RandomInCollection.getRandomInList<Piece>(pieces));
                piece.boardPos = new Vector2(x, y);
                piece.transform.position = getScreenPos(piece);
            }
        }
    }
}
