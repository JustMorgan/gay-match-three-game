﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShapeEnum;

public class BoardController : MonoBehaviour {

    public BoardGrid boardGrid;
    public List<Piece> pieces;
    RefillGrid refillGrid { get; set; }

	// Use this for initialization
	void Start () {
        boardGrid.populateBoard(pieces);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
