﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShapeEnum;

public class BoardController : MonoBehaviour {

    public BoardGrid boardGrid; //the grid this obj controls
    public List<Piece> pieces; //the list of piece prefabs to install
    public Piece grabbedPiece; //the currently selected piece
    RefillGrid refillGrid { get; set; } //the grid of directions that pieces should fall/move

	// Use this for initialization
	void Start () {
        boardGrid.start();
        boardGrid.populateBoard(pieces);
	}
	
    //swap pieces or assign piece to be swapped, or do nothing at all
	public void handleTouchedPiece(Piece other)
    {
        //have a piece selected, and grabbing an adjacent piece, then swap them
        if (grabbedPiece && other && isPieceAdjacent(other, grabbedPiece))
        {
            boardGrid.swapPieces(other, grabbedPiece);
            grabbedPiece.handleSwiped();
            other.handleSwiped();
            grabbedPiece = null;
        //if grabbing the same piece, deselect it
        } else if (grabbedPiece && other && other == grabbedPiece)
        {
            grabbedPiece.handleDeselected();
            grabbedPiece = null;
        //if 
        } else {
            if (grabbedPiece != null)
            {
                grabbedPiece.handleDeselected();
            }
            grabbedPiece = other;
        }

        processAllMatches();
    }

    //moves the grabbed piece in the given direction
    public void swapGrabbedPieceDirection(Vector2 dir)
    {
        if(dir == Vector2.zero)
        {
            return;
        }

        boardGrid.swapPieceInDir(grabbedPiece, dir);

        if (grabbedPiece)
        {
            grabbedPiece.handleDeselected();
            grabbedPiece = null;
        }

        processAllMatches();
    }

    //returns if the two pieces are adjacent to each other
    public bool isPieceAdjacent(Piece a, Piece b)
    {
        return (a.boardPos.x == b.boardPos.x && Mathf.Abs(a.boardPos.y - b.boardPos.y) == 1) ||
            (a.boardPos.y == b.boardPos.y && Mathf.Abs(a.boardPos.x - b.boardPos.x) == 1);
    }

    public void processAllMatches()
    {
        List<MatchObject> matches = new List<MatchObject>();

        //run through grid, for each piece, check right and up if adjacent to piece, then check adjacent piece in same dir
        //add all matched pieces to a set
        for (int x = 0; x < boardGrid.dim.x; x++)
        {
            for (int y = 0; y < boardGrid.dim.y; y++)
            {
                matches.Add(
                    new MatchObject(
                        getMatches(
                            new Vector2(x, y), Vector2.right)));

                matches.Add(
                    new MatchObject(
                        getMatches(
                            new Vector2(x, y), Vector2.up)));
            }
        }

        //after running through everything, process removing and scoring every matched piece
        foreach(MatchObject m in matches)
        {
            m.handleClearingMatch();
        }

        handleRefilling();
    }

    //run through the adjacent matching pieces in a given direction and add them to a global set
    public List<Piece> getMatches(Vector2 pos, Vector2 dir)
    {
        Piece curPiece = boardGrid.getPieceFromPos(pos);
        List<Piece> pieceList = new List<Piece>();

        //if checking off the board, return the list as is
        if ((pos + dir).x >= boardGrid.dim.x || (pos + dir).y >= boardGrid.dim.y)
        {
            pieceList.Add(curPiece);
            return pieceList;
        }

        //get the piece we're comparing to
        Piece newPiece = boardGrid.getPieceFromPos(pos + dir);

        //if either are null, return an empty list
        if (!curPiece || !newPiece)
        {
            pieceList.Add(curPiece);
            return pieceList;
        }
        
        //if the pieces are the same type, keep going
        if (curPiece.shape == newPiece.shape)
        {
            pieceList = getMatches(pos + dir, dir);
        }

        //add this piece, then it returns back up the chain
        pieceList.Add(curPiece);
        return pieceList;

    }

    public void destroyedPiece(Piece piece)
    {
        boardGrid.removePiece(piece);
    }

    public void handleRefilling()
    {
        //loop through every top tile

        //loop through every piece in the board
        for(int x = 0; x < boardGrid.dim.x; x++)
        {
            for(int y = 0; y < boardGrid.dim.y; y++)
            {
                //get the piece
                Piece piece = boardGrid.getPieceFromPos(new Vector2(x, y));
                //if its in the inverse-falling direction of a blank spot
                if (y > 0 && 
                    piece &&
                    !boardGrid.getPieceFromPos(
                        new Vector2(x, y - 1)))
                {
                    boardGrid.swapPieceInDir(piece, Vector2.down);
                }
                //swap it into that dir
            }
        }
    }
}
