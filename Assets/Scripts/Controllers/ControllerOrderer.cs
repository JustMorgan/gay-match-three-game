﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class to set the order of the controller update methods
public class ControllerOrderer : MonoBehaviour
{

    public BoardController board;
    public TouchController touch;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        touch.doTouches();
        //board.processAllMatches();
        //board.handleRefilling();
    }
}
