﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    public BoardController board;

    //called every update, handles the piece that was touched, if any
    public void doTouches()
    {
        if (Input.touchCount < 1)
            return;
        //if a touch happened, and it was the first frame it happened, get the pos, otherwise return
        Touch touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Began)
        {
            getTouchedPiece(touch.position).handleTouched();
            Debug.Log("Touched a piece");
        }

        else if (touch.phase == TouchPhase.Moved) {
            board.swapGrabbedPieceDirection(swipeDirection(touch));
            Debug.Log("Swiped a piece");
        }
    }

    //gets the direction that the touch is in
    private Vector2 swipeDirection(Touch touch)
    {
        if(touch.deltaPosition.magnitude < 6f)
        {
            return Vector2.zero;
        }

        Vector2 dir = touch.deltaPosition;

        if(Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
        {
            if(dir.x > 0)
            {
                return Vector2.right;
            } else
            {
                return Vector2.left;
            }
        } else
        {
            if(dir.y > 0)
            {
                return Vector2.up;
            } else
            {
                return Vector2.down;
            }
        }
    }

    //gets the touch object that was touched
    private TouchObject getTouchedPiece(Vector2 touch)
    {
        //take that pos, move it to world space, get the collider there, and then get the piece attached to it
        Vector2 pos = Camera.main.ScreenToWorldPoint(touch);
        Collider2D col = Physics2D.OverlapPoint(pos);

        if (col != null)
        {
            return col.gameObject.GetComponent<Piece>();
        }

        return null;
    }
}
