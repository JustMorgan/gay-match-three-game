﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardGrid : MonoBehaviour {

    public Vector2 dim; //the size of the board
    public float squareSize; //the size of each piece
    public Vector2 boardOffset; //the relative position of the board
    private Piece[,] pieceGrid; //the actual array of pieces

    public void start()
    {
        pieceGrid = new Piece[(int)dim.x, (int)dim.y];
    }

    //return the piece's position in worldspace
	public Vector2 getScreenPos(Piece piece)
    {
        Vector2 retPos = new Vector2(squareSize * piece.boardPos.x + boardOffset.x, squareSize * piece.boardPos.y + boardOffset.y);

        return retPos;
    }

    //populate the board grid with pieces
    public void populateBoard(List<Piece> pieces)
    {
        for(int x = 0; x < dim.x; x++)
        {
            for(int y = 0; y < dim.y; y++)
            {
                Piece piece = Instantiate<Piece>(RandomInCollection.getRandomInList<Piece>(pieces));
                piece.boardPos = new Vector2(x, y);
                piece.transform.position = getScreenPos(piece);
                piece.board = this;
                pieceGrid[x, y] = piece;
            }
        }
    }

    //swap piece a with b
    public void swapPieces(Piece a, Piece b)
    {
        Vector2 pos = a.boardPos;
        a.boardPos = b.boardPos;
        b.boardPos = pos;
        pieceGrid[(int)a.boardPos.x, (int)a.boardPos.y] = a;
        pieceGrid[(int)b.boardPos.x, (int)b.boardPos.y] = b;
        a.handleMoved();
        b.handleMoved();
    }

    //swap the piece into the specified direction
    public void swapPieceInDir(Piece pc, Vector2 dir)
    {
        if (!pc ||
            pc.boardPos.x + dir.x < 0 ||
            pc.boardPos.x + dir.x >= dim.x ||
            pc.boardPos.y + dir.y < 0 ||
            pc.boardPos.y + dir.y >= dim.y)

            return;

        Piece other = pieceGrid[(int)(pc.boardPos.x + dir.x), (int)(pc.boardPos.y + dir.y)];

        if (other)
        {
            swapPieces(pc, other);
        } else
        {
            movePieceInDir(pc, dir);
        }
    }

    //moves a piece in a dir and replaces it with null
    public void movePieceInDir(Piece pc, Vector2 dir)
    {
        pieceGrid[(int)pc.boardPos.x, (int)pc.boardPos.y] = null;
        pc.boardPos += dir;
        pc.handleMoved();
        pieceGrid[(int)(pc.boardPos.x), (int)(pc.boardPos.y)] = pc;
    }

    //returns the piece at the given coords
    public Piece getPieceFromPos(Vector2 pos)
    {
        return pieceGrid[(int)pos.x, (int)pos.y];
    }

    //removes the piece from the grid
    public void removePiece(Piece piece)
    {
        pieceGrid[(int)piece.boardPos.x, (int)piece.boardPos.y] = null;
    }
}
