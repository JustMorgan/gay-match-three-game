﻿using System.Collections;
using System.Collections.Generic;
using ShapeEnum;
using UnityEngine;

public class Piece : MonoBehaviour, TouchObject {

    public Vector2 boardPos { get; set; } //current board pos
    public Shapes shape; //what shape type the piece is
    public BoardGrid board; //the boardgrid the piece is in
    public BoardController boardController; //the controller for that board
    public float speed; //how quickly the piece moves
    public Glow glow; //the glow object when the piece is selected
    public float selectedScale; //the amount to increase size by when selected
    public float scaleSpeed; //the speed at which the scale increases

    private Vector2 goalPos; //where the piece will be
    private float initScale; //the initial, unselected piece scale
    private float goalScale; //the scale it will be

    //handles a piece being selected
    public void handleTouched()
    {
        glow.turnOn();
        goalScale = initScale + selectedScale;
        boardController.handleTouchedPiece(this);
    }

    public void handleMoved()
    {
        goalPos = board.getScreenPos(this);
    }

    public void handleDeselected()
    {
        glow.turnOff();
        goalScale = initScale;
    }

    public void handleSwiped()
    {
        glow.turnOff();
        transform.localScale = new Vector2(initScale, initScale);
        goalScale = initScale;
    }

	// Use this for initialization
	void Start () {
        initScale = transform.localScale.x;
        board = FindObjectOfType<BoardGrid>();
        boardController = FindObjectOfType<BoardController>();
        goalPos = transform.position;
        goalScale = initScale;
	}
	
	// Update is called once per frame
	void Update () {
        //lerp the position towards the grid position
        transform.position = Vector2.MoveTowards(transform.position, goalPos, Time.deltaTime * speed);

        transform.localScale = Vector2.MoveTowards(transform.localScale, new Vector2(goalScale, goalScale), scaleSpeed * Time.deltaTime);
    }

    void OnDestroy()
    {
        board.removePiece(this);
    }
}
