﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glow : MonoBehaviour
{
    private Light light;

    void Start()
    {
        light = GetComponent<Light>();
    }

    public void turnOn()
    {
        light.enabled = true;
    }

    public void turnOff()
    {
        light.enabled = false;
    }
}
