﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchObject
{
    List<Piece> pieces { get; set; } //list of pieces in this match
    int count; //the number of pieces in the match

    public MatchObject(List<Piece> pieces)
    {
        this.pieces = pieces;
        count = pieces.Count;
    }

    public void handleClearingMatch()
    {
        if (count >= 3)
        {
            foreach (Piece p in pieces)
            {
                if (p)
                    GameObject.DestroyImmediate(p.gameObject);
            }
        }
    }


}
